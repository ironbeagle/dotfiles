# Vim

## Plugins

**Dev Tools**
- [scrooloose/nerdtree](https://github.com/scrooloose/nerdtree): File navigation
- [mhinz/vim-signify](https://github.com/mhinz/vim-signify): Show a diff using Vim its sign column
- [christoomey/vim-tmux-navigator](https://github.com/christoomey/vim-tmux-navigator): Seamless navigation between tmux panes and vim splits
- [vim-syntastic/syntastic](https://github.com/vim-syntastic/syntastic): Syntax checking
- [Shougo/deoplete.nvim](https://github.com/Shougo/deoplete.nvim): Dark powered asynchronous completion framework
  - [neovim](https://neovim.io/)
  - [roxma/vim-hug-neovim-rpc](https://github.com/roxma/vim-hug-neovim-rpc)
  - [roxma/nvim-yarp](https://github.com/roxma/nvim-yarp)
    - [neovim/pynvim](https://github.com/neovim/pynvim)

**Look and feel**
- [vim-airline/vim-airline](https://github.com/vim-airline/vim-airline): Lightweight status bar
- [vim-airline/vim-airline-themes](https://github.com/vim-airline/vim-airline-themes)
- [altercation/vim-colors-solarized](https://github.com/altercation/vim-colors-solarized): Solarized Colorscheme for Vim
- [ryanoasis/vim-devicons](https://github.com/ryanoasis/vim-devicons): Adds file type icons

**Languages**
- [rust-lang/rust.vim](https://github.com/rust-lang/rust.vim)
- [lervag/vimtex](https://github.com/lervag/vimtex)
- [python-mode/python-mode](https://github.com/python-mode/python-mode)
- [LnL7/vim-nix](https://github.com/LnL7/vim-nix)
- [JuliaEditorSupport/julia-vim](https://github.com/JuliaEditorSupport/julia-vim)
