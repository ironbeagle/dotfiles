" File: markdown.vim
" Author: Derek Goddeau <datenstrom@protonmail.com>
" Description: Markdown specific file settings

setlocal spell          " Spellcheck
setlocal wrap           " Word wrap visually
setlocal linebreak      " Only wrap at a character in the `breakat` option
setlocal nolist         " list must be disabled for linebreak to work
