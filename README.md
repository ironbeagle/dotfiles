# Home

Dotfiles and system configuration.

## Making a new machine just like home

```bash
curl -sSL https://gitlab.com/ironbeagle/dotfiles/-/raw/master/bootstrap | bash
install_dev
```

Installs:

- terminator
- alacritty (Pop! OS only)
- zsh
- docker
- gnupg2
- [Rust](https://www.rust-lang.org/en-US/install.html)
- [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh#basic-installation)
- [keybase](https://keybase.io/docs/the_app/install_linux)
- [vim](https://gitlab.com/starshell/scripting/install_scripts) (With Python 3, without ROS)
- ripgrep
- tokei
- tmux
- keybase
- Julia
- Miniconda

Todo:

- [Nix](https://nixos.org/nix/download.html)

## Setup keys

Import PGP keys and run `config crypt unlock`. During `gpg --edit-key` use the command `trust` to set the key trust level.

```bash
keybase pgp export -q 93365252FCC1BC04 --secret -o tmp.secret
cat tmp.secret | gpg --allow-secret-key-import --import
rm tmp.secret
gpg --edit-key 93365252FCC1BC04
git-crypt add-gpg-user 93365252FCC1BC04
git-crypt unlock
```

See [git-crypt](https://www.agwa.name/projects/git-crypt/) for more info.

## Branches

Different branches can be used for different configurations. But to fetch a branch is a little different for a bare repository. To fetch a single branch `dev` run:

```bash
git fetch origin branch dev:dev
```

To get all branches:

```bash
git fetch origin +refs/heads/*:refs/heads/*
```
