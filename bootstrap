#!/bin/bash
#
# Setup dotfiles for a new system.
#
# Entire script is wrapped in functions and called at the end
# so that if any part of the file download fails it will fail
# to execute, rather than partially execute.

# Test if a command exists.
#
# Returns 0 if command exists, otherwise returns 1.
function command_exists
{
    command -v "$@" > /dev/null 2>&1
}

function config_checkout
{
    git --git-dir=$HOME/.cfg/ --work-tree=$HOME checkout
    git --git-dir=$HOME/.cfg/ --work-tree=$HOME config --local status.showUntrackedFiles no
}

# Dotfile collision detection and backup.
#
# Move all dotfiles that will cause collisions during checkout 
# to ~/.config-back/* for safekeeping.
function backup_dotfiles
{
#    mkdir -p $HOME/.config-backup && \
#    config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | \
#    xargs -I{} mv {} $HOME/.config-backup/{}
    for file in $HOME/.{assets,config,fonts,local,nixos,bashrc,dircolors,gitattributes,gitconfig,gitignore,gitlab-ci.yml,gitmessage,gitmodules,tmux.conf,vimrc,wgetrc,zshrc,README.md}; do
        [ -f "$file" ] && \
            mkdir -p $HOME/.config-backup && \
            echo "[*] Backing up $file in ~/.config-backup" && \
            mv "$file" $HOME/.config-backup/
    done;
}

# Bootstrap dotfiles.
#
# - Create `config` alias
# - Create bare git repository in ~/.cfg
# - Backup any existing dotfiles
# - Checkout
function bootstrap
{
    for cmd in {git,grep,egrep,awk,xargs}; do
        echo "[*] Checking for $cmd"
        command_exists "$cmd" || echo "[*] Error: $cmd not found" || exit 1
    done;

    echo "[*] Running setup"
    echo ".cfg" >> $HOME/.gitignore
    git clone --bare --recursive https://ironbeagle@gitlab.com/ironbeagle/dotfiles.git $HOME/.cfg || exit 1
    backup_dotfiles
    config_checkout
    git --git-dir=$HOME/.cfg/ --work-tree=$HOME submodule update --init --recursive
    echo "[*] Setup complete"
    echo "[*] Run 'source ~/.bashrc' or 'source ~/.zshrc' to activate"
}

bootstrap
