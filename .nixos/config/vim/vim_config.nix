{ pkgs }:

let
  vimrc = import ./vimrc.nix { pkgs = pkgs; };
  plugins = import ./plugins.nix { pkgs = pkgs; };
in
with pkgs;
{
  customRC = vimrc.config;

  # Plugins
  #
  # Use the default plugin list shipped with nixpkgs
  # They are installed managed by `vam` (a vim plugin manager)
  # For a list of nixified plugins see:
  # https://github.com/NixOS/nixpkgs/blob/master/pkgs/misc/vim-plugins/vim-plugin-names
  vam = {
    knownPlugins = pkgs.vimPlugins // plugins.customPlugins;
    pluginDictionaries = plugins.std ++ plugins.nvim;
  };
}
