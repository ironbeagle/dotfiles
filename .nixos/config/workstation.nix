# Workstation
#
# This configuration defines the base for any system to be used for
# development.

{ config, pkgs, ... }:

{

  imports =
    [
      ./common.nix
    ];

  environment.shellAliases = {
    hy = "hy3";
    python = "python3";
    pip = "pip3";
    config = "git --git-dir=/home/datenstrom/.cfg/ --work-tree=/home/datenstrom";
  };

  environment.systemPackages = with pkgs; [
    wget
    firefox
    networkmanagerapplet
    tor
    ion
    docker
    zsh
    oh-my-zsh
    dash
    rustup
    cargo
    rustc
    rustfmt
    gcc
    gdb
    gparted
    clang
    gnumake
    gnugrep
    cmake
    python
    python3
    ghc
    clojure
    leiningen 
    hy
    terminator
    git
    git-crypt
    keepassx2
    keybase
    keybase-gui
    vlc
    pciutils
    zip
    unzip
    valgrind
    xdg-user-dirs
    nix-zsh-completions
    zsh-completions
    gnupg
    asciinema
    simplescreenrecorder
    krita
    pkgconfig
    openssl
    alacritty
    byobu
  ];

  programs = {
    zsh = {
      enable = true;
      enableAutosuggestions = true;
      ohMyZsh.enable = true;
      ohMyZsh.theme = "sunrise";
      #shellInit = '' '';
    };
    bash.enableCompletion = true;
    #bash.shellInit = '' '';
    tmux.enable = true;
    command-not-found.enable = true;
  };

  services = {
    printing.enable = true;
    keybase.enable = true;
    kbfs.enable = true;
  };
  hardware.pulseaudio.enable = true;

  fonts = {
    enableFontDir = true;
    fontconfig.enable = true;
    fonts = with pkgs; [
    iosevka
    fira
    fira-mono
    nerdfonts
    powerline-fonts
    source-code-pro
    source-sans-pro
    source-serif-pro
    dejavu_fonts
    ];
  };
  fonts.fontconfig.defaultFonts.monospace = ["Source Code Pro Regular"];
  fonts.fontconfig.defaultFonts.sansSerif = ["Source Sans Pro Regular"];
  fonts.fontconfig.defaultFonts.serif = ["Source Serif Pro Regular"];

  users.defaultUserShell = "/run/current-system/sw/bin/zsh";

}
