{ config, pkgs, ... }:

{

  imports =
    [
      ./x11-common.nix
    ];

  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome3.enable = true;
  services.xserver.desktopManager.default = "gnome3";
  services.xserver.windowManager.xmonad.enable = true;
  services.xserver.desktopManager.kodi.enable = true;

  environment.systemPackages = with pkgs; [
    gnome3.dconf
    gnome3.defaultIconTheme
    gnome3.gnome_themes_standard
  ];

}
