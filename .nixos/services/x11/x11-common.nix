{ config, pkgs, ... }:

{

  services.xserver.enable = true;
  services.xserver.autorun = true;
  services.xserver.layout = "us";
  services.xserver.xkbOptions = "ctrl:nocaps";
  services.xserver.desktopManager.xterm.enable = false;

}
