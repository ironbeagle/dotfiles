# Private Internet Access
#
# FIXME: Currently certs, keys, and credentials must be copied manually.
#
# Start with:
#   sudo systemctl start openvpn-us-east.service

{ config, pkgs, ... }:

let
  sshPort = "22";
  plexWebPort = "32400";
  delugeWebPort = "8112";
  transmissionWebPort = "9091";
  radarrWebPort = "7878";
  sonarrWebPort = "8989";
  lidarrWebPort = "8686";
in
{
  # Add login credentials.
  #
  # Use git-crypt to secure them in repository.
  #systemd.services.openvpn.preStart =
  #''
  #  ${pkgs.stdenv.shell} -c "echo '${pia-auth}' > /root/pia-login.conf"
  #'';

  services.openvpn = {
    servers = {
      us-east = {
        config = ''
          client
          dev tun
          proto udp
          remote us-east.privateinternetaccess.com 1198
          resolv-retry infinite
          nobind
          persist-key
          persist-tun
          cipher aes-128-cbc
          auth sha1
          tls-client
          remote-cert-tls server
          auth-user-pass
          comp-lzo
          verb 1
          reneg-sec 0
          crl-verify /root/crl.rsa.2048.pem
          ca /root/ca.rsa.2048.crt
          disable-occ
          auth-user-pass /root/pia-login.conf
        '';
        autoStart = true;
        up = ''
          echo nameserver $nameserver | ${pkgs.openresolv}/sbin/resolvconf -m 0 -a $dev";

          ip route flush table 100
          ip route flush cache

          ip rule add from 172.17.1.100 table 100
          ip route add table 100 to 172.17.0.0/16 dev enp0s25
          ip route add table 100 default via 172.17.1.1

          iptables -A INPUT -d 172.17.1.100 -p tcp --dport 22 -j ACCEPT
        '';
          #iptables -A INPUT -d 172.17.1.100 -p tcp --dport ${plexWebPort} -j ACCEPT
          #iptables -A INPUT -d 172.17.1.100 -p tcp --dport ${transmissionWebPort} -j ACCEPT
          #iptables -A INPUT -d 172.17.1.100 -p tcp --dport ${radarrWebPort} -j ACCEPT
          #iptables -A INPUT -d 172.17.1.100 -p tcp --dport ${sonarrWebPort} -j ACCEPT
          #iptables -A INPUT -d 172.17.1.100 -p tcp --dport ${lidarrWebPort} -j ACCEPT
          #iptables -A INPUT -d 172.17.1.100 -j DROP
        down = "${pkgs.openresolv}/sbin/resolvconf -d $dev";
      };
    };
  };
}
