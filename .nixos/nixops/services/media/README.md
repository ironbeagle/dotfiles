# A complete media server configuration.

- Currently uses [Plex](https://www.plex.tv/), may be switched to [Emby](https://emby.media/).
- [radarr](https://radarr.video/)
- [sonarr](https://sonarr.tv/)
- [lidarr](https://lidarr.audio/)
- [transmission](https://transmissionbt.com/)
