{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.transmission;
  apparmor = config.security.apparmor.enable;

  homeDir = cfg.home;
  downloadDir = "${homeDir}/Downloads";
  incompleteDir = "${homeDir}/.incomplete";

  settingsDir = "${homeDir}/.config/transmission-daemon";
  settingsFile = pkgs.writeText "settings.json" (builtins.toJSON fullSettings);

  # for users in group "transmission" to have access to torrents
  fullSettings = { umask = 2; download-dir = downloadDir; incomplete-dir = incompleteDir; } // cfg.settings;

  # Directories transmission expects to exist and be ug+rwx.
  directoriesToManage = [ homeDir settingsDir fullSettings.download-dir fullSettings.incomplete-dir ];

  preStart = lib.pkgs.mkForce pkgs.writeScript "transmission-pre-start" ''
    #!${pkgs.runtimeShell}
    set -ex
    chown -R ${cfg.user}:${cfg.group} ${homeDir}
    for DIR in ${escapeShellArgs directoriesToManage}; do
      mkdir -p "$DIR"
      chmod 770 "$DIR"
    done
    cp -f ${settingsFile} ${settingsDir}/settings.json
  '';
in
{
  options = {
    services.transmission = {
      /*
      user = mkOption {
        type = types.str;
        default = "transmission";
        description = "User account under which transmission runs.";
      };

      group = mkOption {
        type = types.str;
        default = "transmission";
        description = "Group under which transmission runs.";
      };
      */

      openFirewall = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Open ports in the firewall for the media server
        '';
      };

    };
  };

  config = mkIf cfg.enable {
  /*
    systemd.services.transmission = pkgs.lib.mkForce {
      serviceConfig.User = cfg.user;
      serviceConfig.Group = cfg.group;
    };

    users.users = mkIf (cfg.user == "transmission") {
      transmission = pkgs.lib.mkForce {
        group = cfg.group;
        uid = config.ids.uids.transmission;
        description = "Transmission BitTorrent user";
        home = homeDir;
        createHome = true;
      };
    };

    users.groups = mkIf (cfg.group == "transmission") {
      transmission = {
      gid = config.ids.gids.transmission;
      };
    };
  */

    networking.firewall = mkIf cfg.openFirewall {
      allowedTCPPorts = [ 51413 cfg.port ];
    };
  };

}
