{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.services.sonarr;
in
{
  options = {
    services.sonarr = {
      openFirewall = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Open ports in the firewall for the media server
        '';
      };

      user = mkOption {
        type = types.str;
        default = "sonarr";
        description = "User account under which sonarr runs.";
      };

      group = mkOption {
        type = types.str;
        default = "sonarr";
        description = "Group under which sonarr runs.";
      };

    };
  };

  config = mkIf cfg.enable {
    systemd.services.sonarr = pkgs.lib.mkForce {
      description = "Sonarr";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      preStart = ''
        test -d /var/lib/sonarr/ || {
          echo "Creating sonarr data directory in /var/lib/sonarr/"
          mkdir -p /var/lib/sonarr/
        }
        chown -R ${cfg.user}:${cfg.group} /var/lib/sonarr/
        chmod 0700 /var/lib/sonarr/
      '';

      serviceConfig = {
        Type = "simple";
        User = cfg.user;
        Group = cfg.group;
        PermissionsStartOnly = "true";
        ExecStart = "${pkgs.sonarr}/bin/NzbDrone --no-browser";
        Restart = "on-failure";
      };
    };

    networking.firewall = mkIf cfg.openFirewall {
      allowedTCPPorts = [ 8989 ];
    };

    users.users = mkIf (cfg.user == "sonarr") {
      sonarr = pkgs.lib.mkForce {
        uid = config.ids.uids.sonarr;
        home = "/var/lib/sonarr";
        group = cfg.group;
        extraGroups = [ "media" ];
      };
    };

    users.groups = mkIf (cfg.group == "sonarr") {
      sonarr = pkgs.lib.mkForce {
        gid = config.ids.gids.sonarr;
      };
    };

  };
}
