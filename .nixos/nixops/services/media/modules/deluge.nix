{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.deluge;
  cfg_web = config.services.deluge.web;
  openFilesLimit = 4096;

in
{
  options = {
    services.deluge = {
      openFirewall = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Open ports in the firewall for the deluge
        '';
      };

      group = mkOption {
        type = types.str;
        default = "deluge";
        description = "Group under which deluge runs.";
      };
    };
  };

  config = mkIf cfg.enable {

    systemd.services.deluged = pkgs.lib.mkForce {
      after = [ "network.target" ];
      description = "Deluge BitTorrent Daemon";
      wantedBy = [ "multi-user.target" ];
      path = [ pkgs.deluge ];
      preStart = ''
        chown -R deluge:${cfg.group} /var/lib/deluge/
        chmod ug+rwx /var/lib/deluge
        chmod o-rwx /var/lib/deluge
      '';

      serviceConfig = {
        ExecStart = "${pkgs.deluge}/bin/deluged -d";
        # To prevent "Quit & shutdown daemon" from working; we want systemd to manage it!
        Restart = "on-success";
        User = "deluge";
        Group = cfg.group;
        LimitNOFILE = cfg.openFilesLimit;
        UMask = "0002";
      };
    };

    networking.firewall = mkIf cfg.openFirewall {
      allowedTCPPorts = [ 8112 ];
      allowedTCPPortRanges = [ { from = 49152; to = 65535; } ];
      allowedUDPPortRanges = [ { from = 49152; to = 65535; } ];
    };

    systemd.services.delugeweb = mkIf cfg_web.enable {
        serviceConfig.Group = pkgs.lib.mkForce cfg.group;
    };

    environment.systemPackages = [ pkgs.deluge ];

    users.groups = mkIf (cfg.group == "deluge") {
      deluge = pkgs.lib.mkForce {
        gid = config.ids.gids.deluge;
      };
    };
  };
}
