# Configuration file for a GitLab runner

{ config, pkgs, lib, ... }:

{

  services.gitlab-runner = {
    enable = true;
    gracefulTermination = true;
    gracefulTimeout = "60min";
  };

  virtualisation.docker = {
    enable = true;
    enableOnBoot = true;
  };

}
