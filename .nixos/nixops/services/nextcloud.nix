# Configuration file for starshell.io
{ config, pkgs, lib, ... }:
{

  networking.hostName = "starshell.io";
  #networking.hostId = "385cabe4";

  imports =
    [
      <nixpkgs/nixos/modules/profiles/headless.nix>
      ./brute-hardware.nix
      ../../modules/nextcloud
      ../../modules/normandy.nix
      ../../modules/jupyter
    ];

  environment.systemPackages = with pkgs; [
    vim
  ];

  networking.firewall.allowPing = true;
  networking.firewall.allowedTCPPorts = [
    22 # SSH
    80 # HTTP
    389 # LDAP
    443 # HTTPS
  ];

  programs.bash.enableCompletion = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.permitRootLogin = "yes";

  # Set your time zone.
  time.timeZone = "America/New_York";

  services.nginx.enable = true;
  services.nextcloud.enable = true;
  services.jupyterhub.enable = true;

  services.postgresql = {
    enable = true;
    package = pkgs.postgresql96;
  };

}
