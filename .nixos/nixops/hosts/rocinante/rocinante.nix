# Configuration file for starshell.io
{ config, pkgs, lib, ... }:

let
  pia-auth = builtins.readFile ../pia.key;
in
{

  networking.hostName = "rocinante";
  time.timeZone = "America/New_York";

  imports =
    [
      <nixpkgs/nixos/modules/profiles/headless.nix>
      ./rocinante-hardware.nix
      ../baseline.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda";

  environment.systemPackages = with pkgs; [
    vim
  ];

  networking.firewall.allowPing = true;
  networking.firewall.allowedTCPPorts = [
    22   # SSH
    80   # HTTP
    443  # HTTPS
    8989 # Sonarr
    7878 # Radarr
    9091 # Transmission
  ];

  programs.bash.enableCompletion = true;


  services.sonarr.enable = true;
  services.radarr.enable = true;

  systemd.services.transmission.serviceConfig = {
    AmbientCapabilities = "cap_net_bind_service";
    PermissionsStartOnly = true;
  };

  # Fix the transmission service default 770 on Downloads.
  systemd.services.transmission.postStart =
    ''
      ${pkgs.stdenv.shell} -c "chmod 777 /var/lib/transmission/plex/Downloads"
    '';

  services.transmission = {
    home = "/var/lib/transmission";
    port = 80;
    settings = {
      download-dir = "/var/lib/transmission/plex/Downloads";
      umask = 000;
      incomplete-dir = "/var/lib/transmission/.incomplete";
      incomplete-dir-enabled = true;
      rpc-whitelist = "127.0.0.1,192.168.*.*";
      setuid = 193;
      setgid = 193;
    };
  };

  systemd.services.openvpn.preStart =
    ''
      ${pkgs.stdenv.shell} -c "echo '${pia-auth}' > /root/pia-login.conf"
    '';

  services.openvpn = {
    enable = true;
    servers = {
      us-east = {
        config = ''
          client
          dev tun
          proto udp
          remote us-east.privateinternetaccess.com 1198
          resolv-retry infinite
          nobind
          persist-key
          persist-tun
          cipher aes-128-cbc
          auth sha1
          tls-client
          remote-cert-tls server
          auth-user-pass
          comp-lzo
          verb 1
          reneg-sec 0
          crl-verify /root/crl.rsa.2048.pem
          ca /root/ca.rsa.2048.crt
          disable-occ
          auth-user-pass /root/pia-login.conf
        '';
        autoStart = true;
        #up = "echo nameserver $nameserver | ${pkgs.openresolv}/sbin/resolvconf -m 0 -a $dev";
        #down = "${pkgs.openresolv}/sbin/resolvconf -d $dev";
      };
    };
  };
}
