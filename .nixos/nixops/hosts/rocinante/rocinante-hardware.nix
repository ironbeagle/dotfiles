{ config, lib, pkgs, ... }:
{
  imports =
    [  <nixpkgs/nixos/modules/installer/scan/not-detected.nix>
    ];

  boot.extraModulePackages = [ ];
  boot.initrd.availableKernelModules = [ "uhci_hcd" "ehci_pci" "ata_piix" "usb_storage" "sd_mod" ];

  services.logind.lidSwitch = "ignore";

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/a20b58b2-daae-4c75-a4fb-0106ed60fbb2";
      fsType = "ext4";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/2e4d720c-3b20-402f-8673-bdf707a8d248";
      fsType = "ext4";
    };

  fileSystems."/var/lib/transmission/plex" =
    { device = "172.17.1.100:/var/lib/plex/media";
      fsType = "nfs";
    };

  nix.maxJobs = lib.mkDefault 2;
  powerManagement.cpuFreqGovernor = "ondemand";
}
