# Configuration file for starshell.io
{ config, pkgs, lib, ... }:
{

  boot.loader.grub = {
    enable = true;
    version = 2;
    devices = [ "/dev/sda" "/dev/sdb" ];
  };

  networking.hostName = "truck";
  time.timeZone = "America/New_York";
  #networking.hostId = "385cabe4";

  imports =
    [
      <nixpkgs/nixos/modules/profiles/headless.nix>
      ./truck-hardware.nix
      ../baseline.nix
    ];

  environment.systemPackages = with pkgs; [
    vim
    btrfs-progs
    btrfs-dedupe
    parted
  ];

  networking.firewall.allowPing = true;
  networking.firewall.allowedTCPPorts = [
    22 # SSH
    80 # HTTP
    389 # LDAP
    443 # HTTPS
  ];

  programs.bash.enableCompletion = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.permitRootLogin = "yes";

  system.stateVersion = "18.03";
}
