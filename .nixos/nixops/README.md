# Nix configuration files for Servers

- `hosts` (server configurations)
- `systems.nix` (for deployment with nixops)
- `test/local.nix` (for testing with nixops)

## Deploy Instructions

**If changing something that could break the network disable headless mode.**

&nbsp;

If ssh methods have changed (password vs. certs) remove old deployements:

```bash
nixops delete --all --force
```

&nbsp;

Create and deploy using the following commands:

```bash
nixops create -d wintermute wintermute.nix
nixops deploy -d wintermute
```

&nbsp;

Modify an existing deployment:

```bash
nixops modify -d wintermute wintermute.nix
nixops deploy -d wintermute
```

## SSH

The keyfile must be used to login, if it is not properly configured in `~/.ssh` it can be specified directly with:

```bash
ssh -i /path/to/id_rsa user@server
```

## Network and Docker

Docker must be stoped if interacting with a 172.0.0.0 network:

```bash
sudo systemctl stop docker
sudo ip link set dev docker0 down
sudo ip link del docker0
```

## Tests

[ssl test](https://www.ssllabs.com/ssltest/analyze.html?d=example.com&latest)

[email test](http://emailsecuritygrader.com/)
